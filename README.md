A simple REST API made in NodeJS (Express), utilizing MongoDB


HOW TO USE:

Make sure you have node installed, and mongod running.

The first thing you should do is to seed your database, and you can do so with POSTing empty request to domain/seed,
Afterwards, feel free to experiment with the routes:

GET /all            gets all tasks

GET /task/:id       gets one task by unique task ID (uuid4 format)

POST /task/         creates a new task

DELETE /task/:id    deletes a task

PUT /task/:id       updates a task (send body: {description:"new description"})

DELETE /all         deletes all tasks