const express = require('express');
const path = require('path');
const router = require('./router')
const mongoose = require('mongoose')

var app = express();

console.log(path.resolve('../'))

var dotenv = require('dotenv').config({path: path.resolve('../.env')});

var db = mongoose.connect(process.env.DB_HOST || "mongodb://localhost:27017/internshipAssignment", { useNewUrlParser: true });


var debug = require('debug')('collectraderappApi:server');
var http = require('http');


app.use(express.json());
app.use(express.urlencoded({ extended: false }));
var port = normalizePort(process.env.PORT || '7999');
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});
app.set('port', port);
app.use('/', router)

var server = http.createServer(app);

server.listen(port);
server.on('error', onError);
server.on('listening', onListening);


function normalizePort(val) {
    var port = parseInt(val, 10);

    if (isNaN(port)) {
        // named pipe
        return val;
    }

    if (port >= 0) {
        // port number
        return port;
    }

    return false;
}

function onError(error) {
    if (error.syscall !== 'listen') {
        throw error;
    }

    var bind = typeof port === 'string'
        ? 'Pipe ' + port
        : 'Port ' + port;

    // handle specific listen errors with friendly messages
    switch (error.code) {
        case 'EACCES':
            console.error(bind + ' requires elevated privileges');
            process.exit(1);
            break;
        case 'EADDRINUSE':
            console.error(bind + ' is already in use');
            process.exit(1);
            break;
        default:
            throw error;
    }
}

function onListening() {
    var addr = server.address();
    var bind = typeof addr === 'string'
        ? 'pipe ' + addr
        : 'port ' + addr.port;
    debug('Listening on ' + bind);
}

