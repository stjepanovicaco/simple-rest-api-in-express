const uuid4 = require('uuid4')
const Task = require('./models/Task')

var tasks = [
    new Task({
        description: "Wait for World Of Warcraft Classic release",
        dateSubmitted: Date.now(),
        taskId: uuid4()
    }),
    new Task({
        description: "Make Horde account",
        dateSubmitted: Date.now(),
        taskId: uuid4()
    }),
    new Task({
        description: "Proudly ride through Durotar",
        dateSubmitted: Date.now(),
        taskId: uuid4()
    }),
    new Task({
        description: "Slay the Alliance. Lok'tar Ogar!",
        dateSubmitted: Date.now(),
        taskId: uuid4()
    })
];

module.exports = tasks;