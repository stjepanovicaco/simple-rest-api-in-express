const mongoose = require('mongoose');

var taskSchema = new mongoose.Schema({
    description: {type: String, required: true},
    dateSubmitted: {type:Date, required: true},
    taskId: {type:String, required: true}
});

module.exports = mongoose.model('Task', taskSchema);