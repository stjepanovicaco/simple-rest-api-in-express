const express = require('express');
const router = express.Router();
const Task = require('./models/Task');
const seeder = require('./seed');
const log = require('simple-node-logger').createSimpleLogger('apilog.log');
const uuid4 = require('uuid4')

router.get('/all', function(req, res){
    Task.find({}).exec(function(err, obj) {
        if(err) {
            log.warn("GET /all: FAIL: Database error");
            res.status(503).send({
                "error" : true,
                "msg" : "Database error"
            })
        } else {
            log.info("GET /all: SUCCESS: Got all");
            res.status(200).send({
                "error" : false,
                "data" : obj
            })
        }
    })
});

router.get('/task/:id', function(req, res) {
    var taskId = req.params.id;
    Task.findOne({taskId: taskId}).exec(function(err, obj) {
        if(err) {
            log.warn("GET /task/:id : FAIL: Database error");
            res.status(503).send({
                "error" : true,
                "msg" : "Database error"
            })
        }
        else if(obj === undefined || obj === null) {
            log.warn("GET /task/:id : FAIL: Task not found");
            res.status(400).send({
                "error" : true,
                "msg" : "Task not found"
            })
        } else {
            log.info("GET /task/:id : SUCCESS: Task returned successfully");
                res.status(200).send({
                    "error" : false,
                    "data" : obj
                })
            }
        }
    )
});

router.post('/task/', function(req, res){
    if(req.body.description === undefined || req.body.description === null) {
        log.warn("POST /task: FAIL: Wrong parameters");
        res.status(400).send({
            "error" : true,
            "msng" : "Wrong params provided"
        })
    } else {
        var newTask = new Task({
            description: req.body.description,
            dateSubmitted: Date.now(),
            taskId: uuid4()
        });
        newTask.save(function(err, obj) {
            if(err) {
                log.warn("POST /task: FAIL: Internal error")
                res.status(500).send({
                    "error" : true,
                    "msg" : "DB error"
                })
            } else {
                log.info("POST /task: SUCCESS: Saved: ", obj.toJSON())
                res.status(200).send({
                    "error" : false,
                    "msg" : "Task saved successfully"
                })
            }
        })
    }
});

router.delete('/task/:id', function(req, res){
    if(req.params.id === undefined || req.params.id === null) {
        log.warn("DELETE /task: FAIL: Internal error");
        res.status(400).send({
            "error" : true,
            "msg" : "Wrong params provided"
        })
    }
    var id = req.params.id;
    Task.deleteOne({taskId: id}, function(err, obj){

        if(!err) {
            if(obj !== undefined && obj.deletedCount !== undefined && obj.deletedCount > 0) {
                log.info("DELETE /task: SUCCESS: Task deleted successfully");
                res.status(200).send({
                    "error" : false,
                    "msg" : "Removed successfully"
                })
            }
            else {
                res.status(200).send({
                    "error" : false,
                    "msg" : "Nothing to delete"
                })
            }

        }
    })
});

router.put('/task/:id', function(req, res){
    console.log(req.params);
    console.log(req.body);
    if(req.params.id === undefined || req.params.id === null) {
        log.warn("PUT /task: FAIL: Wrong params provided");
        res.status(400).send({
            "error": true,
            "msg": "Wrong params provided"
        });
    }
    if(req.body.description === undefined || req.body.description === null) {
        log.warn("PUT /task: FAIL: Body not provided");
        res.status(400).send({
            "error" : true,
            "msg" : "Body not provided"
        })
    } else {
        Task.findOne({taskId:req.params.id}).exec(function(err, obj){
            if(err) {
                log.warn("PUT /task: FAIL: Internal error");
                res.status(500).send({
                    "error" : true,
                    "msg" : "Internal error"
                })
            } else if(obj === undefined || obj === null){
                log.warn("PUT /task: FAIL: Task not found");
                res.status(400).send({
                    "error" : true,
                    "msg" : "Task not found"
                })
            }
            else {
                obj.description = req.body.description;
                obj.save(function(err, obj){
                    if (err) {
                        log.warn("PUT /task: FAIL: Internal error");
                        res.status(500).send({
                            "error" : true,
                            "msg" : "Internal error"
                        })
                    }
                    else {
                        log.info("PUT /task: SUCCESS: Task updated successfully");
                        res.status(200).send({
                            "error" : false,
                            "msg" : "Task updated successfully"
                        })
                    }
                })
            }
        })
    }
});

router.delete('/all', function(req, res){
    Task.deleteMany({}, function(err){
        if(err) {
            log.warn("DELETE /all: FAIL: Internal error")
            res.status(500).send({
                "error" : true,
                "msg" : "Could not delete tasks"
            })
        }
        else {
            log.info("DELETE /all: SUCCESS: All tasks deleted successfully")
            res.status(200).send({
                "error" : false,
                "msg" : "All tasks deleted successfully"
            })
        }
    })
});

router.post('/seed', function(req, res){
    var i = 0;
    console.log(seeder.length)
    seeder.forEach(function(el){
        el.save(function(err, obj){
            if(err) {
                console.log("DB error")
            }  else {
                console.log("Saved:"+obj)
                ++i;
            }
            if(i === seeder.length) {
                log.info("POST /seed: SUCCESS: DB seeded successfully")
                res.status(200).send({
                    "error" : false,
                    "msg" : "DB seeded successfully"
                })
            }
        });
    })
});

module.exports = router;